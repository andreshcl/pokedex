package com.example.jimmyhuertas.pokedex

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PokedexMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokedex_main)

        val TAG = javaClass.simpleName

        val pokemonService = PokemonService.instance
        val pokemonCall = pokemonService.getPokemon()

        pokemonCall.enqueue(object : Callback<PokedexResponse> {
            override fun onFailure(call: Call<PokedexResponse>, t: Throwable) {
                if(call != null){
                    Log.i(TAG,"Call to ${call.request().url()} failed with $t ")
                }
            }

            override fun onResponse(call: Call<PokedexResponse>, response: Response<PokedexResponse>) {
                if (response != null){
                    Log.i(TAG,"Get response with status code ${response.code()}")
                    val body = response.body() as PokedexResponse
                    Log.i(TAG,"Response body: $body")
                }
            }

        })


        val singlePokemonCall = pokemonService.getSinglePokemon("pikachu")
        singlePokemonCall.enqueue(object : Callback<PokedexResponse.PokemonResponse>{
            override fun onFailure(call: Call<PokedexResponse.PokemonResponse>, t: Throwable) {
                if(call != null){
                    Log.i(TAG,"Call to ${call.request().url()} failed with $t ")
                }
            }

            override fun onResponse(
                call: Call<PokedexResponse.PokemonResponse>, response: Response<PokedexResponse.PokemonResponse>) {
                if (response != null){
                    Log.i(TAG,"Get response with status code ${response.code()}")
                    val body = response.body() as PokedexResponse.PokemonResponse
                    Log.i(TAG,"Response body: $body")
                }
            }

        })
    }
}
